package com.diffblue;

public class Goodbye {


  public String bye(String appellation) {

    if (appellation.length() == 0) {
      return "Goodbye Nameless Person";
    }
    return "goodbye " + appellation;
  }


  public String zaijian(String name) {
    if (name.length() == 0) {
      return "Zaijian ni ming de ren";
    } else if ( name.equals("Chen")){
      return "sheng ri kuai le";
    }
    return "Zaijian " + name;
  }

}
