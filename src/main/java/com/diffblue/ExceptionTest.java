package com.diffblue;



public class ExceptionTest {


  int size;

  public ExceptionTest(int size) throws IllegalArgumentException {

    if (size < 5) {
      throw new IllegalArgumentException("Size must be greater or equal to  5");
    } else {
      this.size = size;
    }
  }

  }

