package com.diffblue;

public class Currency {

  public String getSymbol(String symbol) {
    if (symbol.equals("£")) {
      return "GBP Selected";
    } else if (symbol.equals("$")) {
      return "USD Selected";
    } else if (symbol.equals("€")) {
      return "Euro Selected";
    } else {
      return "No Matching Symbol";
    }
  }

  public double convert(double val){
      double exchangeRate = 0.79;
    return  val * exchangeRate ;
  }

}
