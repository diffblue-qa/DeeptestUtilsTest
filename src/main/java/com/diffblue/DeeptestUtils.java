package com.diffblue;

import com.diffblue.deeptestutils.CompareWithFieldList;
import java.util.HashMap;

public class DeeptestUtils {


    public void compare(HashMap h1,HashMap h2){

      CompareWithFieldList.compare(h1,h2);

    }


    public static void main(String[]  args){

      DeeptestUtils d = new DeeptestUtils();

      HashMap<Integer,String>  h1 = new HashMap<Integer, String>();
      HashMap<String,String>  h2 = new HashMap<String, String>();

      try{
        d.compare(h1,h2);
      }catch( Exception e){
        System.out.println(e.getStackTrace()) ;
      }


    }


}
