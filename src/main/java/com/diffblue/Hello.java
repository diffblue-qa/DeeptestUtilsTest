package com.diffblue;

public class Hello {

  public String hi(String name) {
    if (name.length() == 0) {
      return "hi  Man with No Name";
    } else if (name.length() < 5) {
      return "hi short " + name;
    } else if (name.length() < 10) {
      return "hi long " + name;
    } else {
      return "hi " + name;
    }
  }

  public String bonjour(String nom) {
    if (nom.length() == 0) {
      return "Bonjour Monsieur";
    } else if (nom.length() < 5) {
      return "Bonjur petite " + nom;
    } else if (nom.length() < 15) {
      return "Bonjour demi " + nom;
    } else {
      return "Bonjour " + nom;
    }
  }

  public String hi2(String name){
    if (name.length() == 0) {
      return "Hi , Man with no name";
    } else {
      return "Hi " + name;
    }
  }


}
