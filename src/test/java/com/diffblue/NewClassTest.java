package com.diffblue;

import com.diffblue.NewClass;
import com.diffblue.deeptestutils.CompareWithFieldList;
import com.diffblue.deeptestutils.Reflector;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestWatchman;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.junit.runners.model.FrameworkMethod;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class NewClassTest {

  @org.junit.Rule
  public ExpectedException thrown = ExpectedException.none();

  /* testedClasses: NewClass */


  /*
   * Test generated by Diffblue Deeptest.
   * This test case covers the entire method.
   */

  @Test
  public void constructor1() throws Throwable {

    // Act, creating object to test constructor
    com.diffblue.NewClass instance = new com.diffblue.NewClass();

    // Method returns void, testing that no exception is thrown

  }

  /*
   * Test generated by Diffblue Deeptest.
   * This test case covers:
   *  - conditional line 7 branch to line 17
   */

  @Test
  public void countletters1() throws Throwable {

    // Arrange
    NewClass objectUnderTest = ((NewClass)Reflector.getInstance("com.diffblue.NewClass"));
    String str = "\"#######";

    // Act
    int retval = objectUnderTest.countletters(str);

    // Assert result
    Assert.assertEquals(8, retval);

  }

  /*
   * Test generated by Diffblue Deeptest.
   * This test case covers:
   *  - conditional line 7 branch to line 9
   *  - com/diffblue/NewClass.java:10: loop: 2 iterations
   *  - iteration 1
   *     - conditional line 11 branch to line 12
   *  - iteration 2
   *     - conditional line 11 branch to line 10
   */

  @Test
  public void countletters3() throws Throwable {

    // Arrange
    NewClass objectUnderTest = ((NewClass)Reflector.getInstance("com.diffblue.NewClass"));
    String str = "` ";

    // Act
    int retval = objectUnderTest.countletters(str);

    // Assert result
    Assert.assertEquals(1, retval);

  }

  /*
   * Test generated by Diffblue Deeptest.
   * This test case covers:
   *  - conditional line 7 branch to line 9
   *  - com/diffblue/NewClass.java:10: loop: 1 iterations
   *  - iteration 1
   *     - conditional line 11 branch to line 10
   */

  @Test
  public void countletters2() throws Throwable {

    // Arrange
    NewClass objectUnderTest = ((NewClass)Reflector.getInstance("com.diffblue.NewClass"));
    String str = " ";

    // Act
    int retval = objectUnderTest.countletters(str);

    // Assert result
    Assert.assertEquals(0, retval);

  }

  /*
   * Test generated by Diffblue Deeptest.
   * This test case covers:
   *  - conditional line 39 branch to line 41
   *  - conditional line 41 branch to line 44
   */

  @Test
  public void getResult1() throws Throwable {

    // Arrange
    NewClass objectUnderTest = ((NewClass)Reflector.getInstance("com.diffblue.NewClass"));
    int a = 0;
    int b = 0;
    String type = "SWG";

    // Act
    int retval = objectUnderTest.getResult(a, b, type);

    // Assert result
    Assert.assertEquals(0, retval);

  }

  /*
   * Test generated by Diffblue Deeptest.
   * This test case covers:
   *  - conditional line 39 branch to line 41
   *  - conditional line 41 branch to line 42
   */

  @Test
  public void getResult2() throws Throwable {

    // Arrange
    NewClass objectUnderTest = ((NewClass)Reflector.getInstance("com.diffblue.NewClass"));
    int a = 0;
    int b = 536_870_912;
    String type = "SWG";

    // Act
    int retval = objectUnderTest.getResult(a, b, type);

    // Assert result
    Assert.assertEquals(536_870_912, retval);

  }

  /*
   * Test generated by Diffblue Deeptest.
   * This test case covers:
   *  - conditional line 39 branch to line 40
   */

  @Test
  public void getResult3() throws Throwable {

    // Arrange
    NewClass objectUnderTest = ((NewClass)Reflector.getInstance("com.diffblue.NewClass"));
    int a = 0;
    int b = 536_870_912;
    String type = "sum";

    // Act
    int retval = objectUnderTest.getResult(a, b, type);

    // Assert result
    Assert.assertEquals(536_870_912, retval);

  }

  /*
   * Test generated by Diffblue Deeptest.
   * This test case covers:
   *  - case 1 of switch on line 22
   */

  @Test
  public void selection1() throws Throwable {

    // Arrange
    NewClass objectUnderTest = ((NewClass)Reflector.getInstance("com.diffblue.NewClass"));
    int choice = 1;

    // Act
    String retval = objectUnderTest.selection(choice);

    // Assert result
    Assert.assertEquals("Search", retval);

  }

  /*
   * Test generated by Diffblue Deeptest.
   * This test case covers:
   *  - case 2 of switch on line 22
   */

  @Test
  public void selection4() throws Throwable {

    // Arrange
    NewClass objectUnderTest = ((NewClass)Reflector.getInstance("com.diffblue.NewClass"));
    int choice = 2;

    // Act
    String retval = objectUnderTest.selection(choice);

    // Assert result
    Assert.assertEquals("Login", retval);

  }

  /*
   * Test generated by Diffblue Deeptest.
   * This test case covers the entire method.
   */

  @Test
  public void selection2() throws Throwable {

    // Arrange
    NewClass objectUnderTest = ((NewClass)Reflector.getInstance("com.diffblue.NewClass"));
    int choice = 16_386;

    // Act
    String retval = objectUnderTest.selection(choice);

    // Assert result
    Assert.assertEquals("Try Again", retval);

  }

  /*
   * Test generated by Diffblue Deeptest.
   * This test case covers:
   *  - case 3 of switch on line 22
   */

  @Test
  public void selection5() throws Throwable {

    // Arrange
    NewClass objectUnderTest = ((NewClass)Reflector.getInstance("com.diffblue.NewClass"));
    int choice = 3;

    // Act
    String retval = objectUnderTest.selection(choice);

    // Assert result
    Assert.assertEquals("Exit", retval);

  }

  /*
   * Test generated by Diffblue Deeptest.
   * This test case covers:
   *  - case 4 of switch on line 22
   */

  @Test
  public void selection3() throws Throwable {

    // Arrange
    NewClass objectUnderTest = ((NewClass)Reflector.getInstance("com.diffblue.NewClass"));
    int choice = 4;

    // Act
    String retval = objectUnderTest.selection(choice);

    // Assert result
    Assert.assertEquals("Backdoor", retval);

  }
}
